﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;

namespace QuickScope
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly ItemService _itemService;
        
        public MainWindow()
        {
            InitializeComponent();
            _itemService = new ItemService();
            
            ItemsList.ItemsSource = _itemService.GetAll();
        }

        public void ShowApp()
        {
            Show();
            WindowState = WindowState.Normal;
            TextBox.Focus();
            Popup.IsOpen = true;
        }

        private void HideApp()
        {
            Popup.IsOpen = false;
            Hide();
            WindowState = WindowState.Minimized;
            ShowInTaskbar = false;
        }

        private void TextBox_OnKeyDown(object sender, KeyEventArgs e)
        {
            var selection = TextBox.Text;

            if (e.Key == Key.Enter)
            {
                var item = _itemService.GetBySearchName(selection);
                if (item != null)
                {
                    //Runner.Run(item);
                    HideApp();
                }
            }

            if (e.Key == Key.Escape)
            {
                if (!string.IsNullOrWhiteSpace(selection))
                    TextBox.Text = string.Empty;
                else
                {
                    Popup.IsOpen = false;
                    HideApp();
                }
            }
        }

        private void Textbox_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            var selection = TextBox.Text;

            var items = _itemService.GetByFilter(selection);
            
            var anyMatches = items != null && items.Any();

            ItemsList.ItemsSource = anyMatches ? items : null;

            ItemsList.Items.Refresh();
            Popup.IsOpen = anyMatches;
        }
  
    }
}