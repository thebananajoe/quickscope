﻿using System.Windows.Media.Imaging;
using QuickScope.Extensions;

namespace QuickScope.Icon
{
    public class FolderIconFetcher : IconFetcherBase, IIconFetcher
    {
        public FolderIconFetcher() 
            : base(ItemType.Folder)
        {
        }

        public BitmapImage Get(string path)
        {
            var icon = IconHelper.GetFolderIcon(path, IconHelper.IconSize.Large, IconHelper.FolderType.Open);

            return icon == null ? GetDefault() : icon.ToBitmapImage();
        }
    }
}