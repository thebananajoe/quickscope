﻿using System;
using System.Reflection;
using System.Windows.Media.Imaging;

namespace QuickScope.Icon
{
    public abstract class IconFetcherBase
    {
        protected IconFetcherBase(ItemType type)
        {
            Type = type;
        }

        public ItemType Type { get; }

        protected BitmapImage GetDefault()
        {
            var path = $"pack://application:,,,/{Assembly.GetExecutingAssembly().GetName().Name};component/Ressources/Default_{Type}.png";

            return new BitmapImage(new Uri(path));
        }
    }
}