﻿using System.Collections.Generic;

namespace QuickScope.Icon
{
    public class IconFetcherFactory
    {
        private readonly List<IIconFetcher> _iconFetchers;

        public IconFetcherFactory()
        {
            _iconFetchers = new List<IIconFetcher>
            {
                new FaviconFetcher(),
                new FolderIconFetcher(),
                new FileIconFetcher()
            };
        }

        public IIconFetcher GetIconFetcher(ItemType type)
        {
            return _iconFetchers.Find(iconFetcher => iconFetcher.Type == type);
        }
    }
}