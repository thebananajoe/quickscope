﻿using System;
using System.IO;
using System.Net;
using System.Windows.Media.Imaging;
using Elmah.Io.FaviconLoader;

namespace QuickScope.Icon
{
    public class FaviconFetcher : IconFetcherBase, IIconFetcher
    {
        public FaviconFetcher() 
            : base(ItemType.Website)
        {
        }

        public BitmapImage Get(string path)
        {
            var faviconUri = GetFaviconUri(new Uri(path));

            if (faviconUri == null)
                return GetDefault();

            var webClient = new WebClient();
            var data = webClient.DownloadData(faviconUri);

            return ToBitmapImage(data);
        }

        private static Uri GetFaviconUri(Uri uri)
        {
            var favicon = new Favicon();

            try
            {
                return favicon.Load(uri);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static BitmapImage ToBitmapImage(byte[] bytes)
        {
            var stream = new MemoryStream(bytes);
            var image = new BitmapImage();
            image.BeginInit();
            image.StreamSource = stream;
            image.EndInit();
            return image;
        }
    }
}