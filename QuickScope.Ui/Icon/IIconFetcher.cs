﻿using System.Windows.Media.Imaging;

namespace QuickScope.Icon
{
    public interface IIconFetcher
    {
        ItemType Type { get; }

        BitmapImage Get(string path);
    }
}