﻿using System.Windows.Media.Imaging;
using QuickScope.Extensions;

namespace QuickScope.Icon
{
    public class FileIconFetcher : IconFetcherBase, IIconFetcher
    {
        public FileIconFetcher() 
            : base(ItemType.Application)
        {
        }

        public BitmapImage Get(string path)
        {
            var icon = IconHelper.GetFileIcon(path, IconHelper.IconSize.Large, true);

            return icon == null ? GetDefault() : icon.ToBitmapImage();
        }
    }
}