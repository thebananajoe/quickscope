﻿using System.Collections.Concurrent;
using System.Windows.Media.Imaging;

namespace QuickScope.Icon
{
    public class IconService
    {
        private readonly IconFetcherFactory _iconFetcherFactory;
        private readonly ConcurrentDictionary<int, BitmapImage> _iconCache;

        public IconService()
        {
            _iconFetcherFactory = new IconFetcherFactory();
            _iconCache = new ConcurrentDictionary<int, BitmapImage>();
        }

        public BitmapImage GetIcon(Item item)
        {
            if (_iconCache.ContainsKey(item.Id))
            {
                return _iconCache[item.Id];
            }

            var iconFetcher = _iconFetcherFactory.GetIconFetcher(item.Type);

            var icon = iconFetcher.Get(item.Path);

            _iconCache.GetOrAdd(item.Id, icon);

            return icon;
        }
    }
}