﻿using System.Diagnostics;

namespace QuickScope
{
    public class Runner
    {
        private readonly ItemService _itemService;

        public Runner()
        {
            _itemService = new ItemService();
        }

        private static void OpenExplorer(string path)
        {
            Process.Start("explorer.exe", path);
        }

        public void Run(int id)
        {
            var item = _itemService.GetById(id);
            Run(item);
        }

        public static void Run(Item item)
        {
            switch (item.Type)
            {
                case ItemType.Folder:
                {
                    OpenExplorer(item.Path);
                    break;
                }
                case ItemType.Website:
                case ItemType.Application:
                {
                    Process.Start(item.Path);
                    break;
                }
            }
        }
    }
}