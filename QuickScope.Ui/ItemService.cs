﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using QuickScope.Extensions;
using QuickScope.Icon;

namespace QuickScope
{
    public class ItemService
    {
        private readonly IconService _iconService;
        private readonly IEnumerable<Item> _items;

        public ItemService()
        {
            _iconService = new IconService();
            var json = File.ReadAllText("settings.json");
            _items = JsonConvert.DeserializeObject<IEnumerable<Item>>(json);
        }

        public Item GetBySearchName(string filter)
        {
            return _items.FirstOrDefault(i => i.SearchName.Contains(filter));
        }

        public IList<ItemViewModel> GetAll()
        {
            var items = _items.OrderBy(i => i.SearchName);
            return items.Select(ToViewModel).ToList();
        }
        
        public IList<ItemViewModel> GetByFilter(string filter)
        {
            if (string.IsNullOrEmpty(filter))
                return GetAll();

            var items = _items.Where(i => i.SearchName.Contains(filter, StringComparison.OrdinalIgnoreCase)).OrderBy(i => i.SearchName).ToList();
            items.AddRange(_items.Where(i => i.Tags.Any(tag => tag.Contains(filter, StringComparison.OrdinalIgnoreCase))));
            return items.Select(ToViewModel).ToList();
        }

        private ItemViewModel ToViewModel(Item item)
        {
            var result = new ItemViewModel
            {
                Id = item.Id,
                SearchName = item.SearchName,
                IconSource = _iconService.GetIcon(item)
            };

            return result;
        }

        public Item GetById(int id)
        {
            return _items.FirstOrDefault(item => item.Id == id);
        }
    }
}