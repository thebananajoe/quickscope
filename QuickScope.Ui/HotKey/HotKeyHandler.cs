﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Interop;

namespace QuickScope.HotKey
{
    public class HotKeyHandler
    {
        private HwndSource _source;
        private const int HOTKEY_ID = 9000;

        [DllImport("User32.dll")]
        private static extern bool RegisterHotKey([In] IntPtr hWnd, [In] int id, [In] uint fsModifiers, [In] uint vk);

        [DllImport("User32.dll")]
        private static extern bool UnregisterHotKey([In] IntPtr hWnd, [In] int id);

        public void RegisterHotKey(IntPtr handle)
        {
            _source = HwndSource.FromHwnd(handle);
            _source.AddHook(HwndHook);
            const uint VK_F10 = 0x79;
            const uint MOD_CTRL = 0x0002;
            if (!RegisterHotKey(handle, HOTKEY_ID, MOD_CTRL, VK_F10))
            {
                // handle error
            }
        }

        public void UnregisterHotKey(IntPtr handle)
        {
            _source.RemoveHook(HwndHook);
            _source = null;
            UnregisterHotKey(handle, HOTKEY_ID);
        }

        private IntPtr HwndHook(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            const int WM_HOTKEY = 0x0312;
            switch (msg)
            {
                case WM_HOTKEY:
                    switch (wParam.ToInt32())
                    {
                        case HOTKEY_ID:
                            OnHotKeyPressed?.Invoke(null, EventArgs.Empty);
                            handled = true;
                            break;
                    }
                    break;
            }
            return IntPtr.Zero;
        }

        public EventHandler OnHotKeyPressed;
    }
}