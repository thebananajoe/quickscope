﻿using System.Windows.Media.Imaging;

namespace QuickScope
{
    public class ItemViewModel
    {
        public int Id { get; set; }

        public string SearchName { get; set; } 

        public BitmapImage IconSource { get; set; }
    }
}