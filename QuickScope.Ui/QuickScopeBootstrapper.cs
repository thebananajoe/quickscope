﻿using System;
using System.Windows;
using Caliburn.Micro;
using QuickScope.HotKey;
using QuickScope.ViewModels;

namespace QuickScope
{
    public class QuickScopeBootstrapper : BootstrapperBase
    {
        private NavigatorViewModel _navigatorViewModel;
        private readonly WindowManager _windowManager;
        private readonly HotKeyHandler _hotKeyHandler;
        
        public QuickScopeBootstrapper()
        {
            Initialize();
            _windowManager = new WindowManager();
            _hotKeyHandler = new HotKeyHandler();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            _navigatorViewModel = IoC.Get<NavigatorViewModel>();
            _navigatorViewModel.HandleInitialized += NavigatorViewModelOnHandleInitialized;
            _windowManager.ShowWindow(_navigatorViewModel);
        }

        private void NavigatorViewModelOnHandleInitialized(object sender, EventArgs eventArgs)
        {
            var handle = _navigatorViewModel.Handle;
            _hotKeyHandler.RegisterHotKey(handle);
            _hotKeyHandler.OnHotKeyPressed +=OnHotKeyPressed;
        }

        private void OnHotKeyPressed(object sender, EventArgs eventArgs)
        {
            _navigatorViewModel.Show();
        }

        protected override void OnExit(object sender, EventArgs e)
        {
            _hotKeyHandler.UnregisterHotKey(_navigatorViewModel.Handle);
            base.OnExit(sender, e);
        }
    }
}