﻿using System;
using System.Windows;
using System.Windows.Data;

namespace QuickScope.Views
{
    /// <summary>
    /// Interaction logic for NavigatorView.xaml
    /// </summary>
    public partial class NavigatorView : Window
    {
        public NavigatorView()
        {
            InitializeComponent();
        }
        
        private void NavigatorView_OnStateChanged(object sender, EventArgs e)
        {
            if (WindowState == WindowState.Normal)
                Activate();
        }
    }
}
