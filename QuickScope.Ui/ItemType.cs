﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace QuickScope
{
    public enum ItemType
    {
        Folder,
        Application,
        Website
    }
}