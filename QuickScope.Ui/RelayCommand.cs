﻿using System;
using System.Windows.Input;

namespace QuickScope
{
    public class RelayCommand : ICommand
    {
        private readonly Predicate<object> _canExecute;
        private readonly Action<object> _execute;
        private readonly bool _canExecuteIsImplemented;

        public RelayCommand(Predicate<object> canExecute, Action<object> execute)
        {
            _canExecute = canExecute;
            _canExecuteIsImplemented = true;
            _execute = execute;
        }

        public RelayCommand(Action<object> execute)
        {
            _canExecute = o => true;
            _execute = execute;
        }

        public RelayCommand(Action execute)
        {
            _canExecute = o => true;
            _execute = o => execute.Invoke();
        }

        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            _execute(parameter);
        }
    }
}
