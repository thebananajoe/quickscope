﻿using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media.Imaging;

namespace QuickScope.Extensions
{
    public static class IconExtensions
    {
        public static BitmapImage ToBitmapImage(this System.Drawing.Icon icon)
        {
            var bitmap = icon.ToBitmap();

            using (var memory = new MemoryStream())
            {
                bitmap.Save(memory, ImageFormat.Png);
                memory.Position = 0;

                var bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();

                return bitmapImage;
            }
        }
    }
}