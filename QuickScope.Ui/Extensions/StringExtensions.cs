﻿using System;

namespace QuickScope.Extensions
{
    public static class StringExtensions
    {
        private const int MaxMatchDistance = 5;

        public static bool IsSimilarTo(this string value, string compareValue)
        {
            return LevenshteinDistance(value, compareValue) <= MaxMatchDistance;
        }

        private static int LevenshteinDistance(this string input, string comparedTo, bool caseSensitive = false)
        {
            if (string.IsNullOrWhiteSpace(input) || string.IsNullOrWhiteSpace(comparedTo)) return -1;
            if (!caseSensitive)
            {
                input = input.ToLower();
                comparedTo = comparedTo.ToLower();
            }
            int inputLen = input.Length;
            int comparedToLen = comparedTo.Length;

            int[,] matrix = new int[inputLen, comparedToLen];

            //initialize
            for (int i = 0; i < inputLen; i++) matrix[i, 0] = i;
            for (int i = 0; i < comparedToLen; i++) matrix[0, i] = i;

            //analyze
            for (int i = 1; i < inputLen; i++)
            {
                var si = input[i - 1];
                for (int j = 1; j < comparedToLen; j++)
                {
                    var tj = comparedTo[j - 1];
                    int cost = (si == tj) ? 0 : 1;

                    var above = matrix[i - 1, j];
                    var left = matrix[i, j - 1];
                    var diag = matrix[i - 1, j - 1];
                    var cell = FindMinimum(above + 1, left + 1, diag + cost);

                    //transposition
                    if (i > 1 && j > 1)
                    {
                        var trans = matrix[i - 2, j - 2] + 1;
                        if (input[i - 2] != comparedTo[j - 1]) trans++;
                        if (input[i - 1] != comparedTo[j - 2]) trans++;
                        if (cell > trans) cell = trans;
                    }
                    matrix[i, j] = cell;
                }
            }
            return matrix[inputLen - 1, comparedToLen - 1];
        }

        private static int FindMinimum(params int[] p)
        {
            if (null == p) return int.MinValue;
            int min = int.MaxValue;
            for (int i = 0; i < p.Length; i++)
            {
                if (min > p[i]) min = p[i];
            }
            return min;
        }

        public static bool Contains(this string value, string compareValue, StringComparison comp = StringComparison.OrdinalIgnoreCase)
        {
            return value.IndexOf(compareValue, comp) >= 0;
        }
    }
}