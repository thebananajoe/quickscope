﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Threading;
using Caliburn.Micro;
using QuickScope.Views;

namespace QuickScope.ViewModels
{
    public class NavigatorViewModel : Screen
    {
        private readonly ItemService _itemService;
        private readonly Runner _runner;

        public NavigatorViewModel()
        {
            _itemService = new ItemService();
            _runner = new Runner();
            WindowState = WindowState.Minimized;
            Items = new ObservableCollection<ItemViewModel>();
        }

        public bool FocusTextbox { get; set; }

        public WindowState WindowState { get; set; }

        public ObservableCollection<ItemViewModel> Items { get; set; }

        public ItemViewModel SelectedItem { get; set; }

        private string _searchText;

        public string SearchText
        {
            get => _searchText;
            set
            {
                _searchText = value;
                NotifyOfPropertyChange(() => SearchText);
                UpdateItemList(_searchText);
            }
        }

        public ICommand RunCommand => new RelayCommand(Run);

        public ICommand SelectNextCommand => new RelayCommand(SelectNextItem);

        public ICommand SelectPreviousCommand => new RelayCommand(SelectPreviousItem);

        public ICommand EscapeCommand => new RelayCommand(Escape);

        public IntPtr Handle { get; private set; }

        protected override void OnViewLoaded(object view)
        {
            var navigatorView = view as NavigatorView;
            var interopHelper = new WindowInteropHelper(navigatorView);
            var handle = interopHelper.Handle;
            Handle = handle;
            HandleInitialized?.Invoke(this, EventArgs.Empty);
            base.OnViewLoaded(view);
        }

        private void Run()
        {
            _runner.Run(SelectedItem.Id);
            Hide();
        }

        private void SelectNextItem()
        {
            if (Items.Any())
            {
                var newIndex = Items.IndexOf(SelectedItem) + 1;
                if (newIndex < Items.Count)
                {
                    SelectedItem = Items[newIndex];
                    NotifyOfPropertyChange(() => SelectedItem);
                }
            }
        }

        private void SelectPreviousItem()
        {
            if (Items.Any())
            {
                var newIndex = Items.IndexOf(SelectedItem) - 1;
                if (newIndex >= 0)
                {
                    SelectedItem = Items[newIndex];
                    NotifyOfPropertyChange(() => SelectedItem);
                }
            }
        }

        private void Escape()
        {
            if (string.IsNullOrEmpty(SearchText))
                Hide();
            else
                SearchText = string.Empty;
        }

        public void Show()
        {
            WindowState = WindowState.Normal;
            NotifyOfPropertyChange(() => WindowState);
            FocusTextbox = true;
            NotifyOfPropertyChange(() => FocusTextbox);
            UpdateItemList(string.Empty);
        }

        private void UpdateItemList(string searchText)
        {
            Items.Clear();

            var updater = new ItemUpdater();
            updater.ItemsUpdated += (s, e) => {
                OnUIThread(() =>
                {
                    var items = ((ItemsUpdatedEventArgs) e).Items;
                    foreach (var item in items)
                    {
                        Items.Add(item);
                    }
                    NotifyOfPropertyChange(() => Items);
                });
            };
            var updateThread = new Thread(updater.GetItems);
            updateThread.Start(searchText);
        }

        private void Hide()
        {
            WindowState = WindowState.Minimized;
            NotifyOfPropertyChange(() => WindowState);
        }
        
        public delegate void HandleInitializedHandler(object sender, EventArgs e);

        public event HandleInitializedHandler HandleInitialized;
    }

    public class ItemUpdater
    {
        public event EventHandler ItemsUpdated;
        private readonly ItemService _itemService;

        public ItemUpdater()
        {
            _itemService = new ItemService();
        }

        public void GetItems(object searchText)
        {
            var items = _itemService.GetByFilter((string)searchText);

            ItemsUpdated?.Invoke(this, new ItemsUpdatedEventArgs(items));
        }
    }

    public class ItemsUpdatedEventArgs : EventArgs
    {
        public ItemsUpdatedEventArgs(IList<ItemViewModel> items)
        {
            Items = items;
        }

        public IList<ItemViewModel> Items { get; }
    }
}