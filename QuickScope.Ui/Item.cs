﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace QuickScope
{
    public class Item
    {
        public int Id { get; set; }

        public string SearchName { get; set; }

        public string Path { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public ItemType Type { get; set; }

        public IEnumerable<string> Tags { get; set; }
    }
}